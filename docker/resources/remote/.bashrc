export HISTTIMEFORMAT="%d/%m/%y %T "
export PS1='\u@\h:\W \$ '
alias ls='ls --color=auto'
alias l='ls -lah'
alias la='ls -A'
alias ll='ls -alF'
source /etc/profile.d/bash_completion.sh
