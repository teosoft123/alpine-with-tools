#!/usr/bin/env bash

docker build --tag alpine-with-tools -f docker/Dockerfile .
docker tag alpine-with-tools teosoft/alpine-with-tools:latest
