# README #

### Alpine container with troubleshooting tools ###

* bash
* bash-completions
* kubectl
* curl
* tree 
* host (+bind-tools)

### Add this to .bashrc

    export HISTTIMEFORMAT="%d/%m/%y %T "
    export PS1='\u@\h:\W \$ '
    alias l='ls -lah'
    alias la='ls -A'
    alias ll='ls -alF'
    alias ls='ls --color=auto'
    source /etc/profile.d/bash_completion.sh
